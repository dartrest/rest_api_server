import 'dart:async';
import 'dart:convert';
import 'dart:io' hide HttpException;

import 'package:path/path.dart';
import 'package:shelf/shelf.dart' as shelf;

import 'middleware.dart';
import 'jwt.dart';

export 'jwt.dart' show Jwt;

/// AuthMiddleware
///
/// The middleware is responsible for user authentication
///
/// [loginPaths] should contain paths to login api-method.
///
/// If login successful login api-method should return shelf [Response] object with `status`
/// 200-Ok and `context` containing following [Map]:
///
///     {
///       subject: <userId>,
///       payload: {
///         // any additional information
///       }
///     }
///
/// If authentication was successful ([Response] `status` 200-Ok), then response `Authorization` header
/// will contain jwt which must be sent from cleint along with each subsequent requests
///
/// Jwt has expiry period after that jwt is not valid. If request has valid jwt then
/// response will contain new jwt with new expiry date and time.
///
/// If jwt is not valid request will not proceed to the handler
class AuthMiddleware implements Middleware {
  final Map<String, List<String>> _loginPaths;
  final Map<String, List<RegExp>> _exclude;
  final Jwt _jwt;

  /// Creates AuthMiddleware
  ///
  /// [loginPaths] paths to the login api-method.
  ///
  /// Example:
  ///
  ///     loginPaths: {
  ///                   'POST': [
  ///                     '/api/login'
  ///                    ]
  ///                 }
  ///
  /// [exclude] - [Map] object describing api-paths which does not require authentication.
  ///
  /// Example:
  ///     {
  ///       'GET': [
  ///         '/path/to/resource1',
  ///         '/path/to/resource2',
  ///         ...
  ///       ],
  ///       'POST': [
  ///         '/api/login',
  ///         ...
  ///       ],
  ///       ...
  ///     }
  /// Names of http-methods must be in upper case
  ///
  /// [jwt] JWT-manager
  AuthMiddleware(
      {Map<String, List<String>> loginPaths = const {},
      Map<String, List<String>> exclude = const {},
      Jwt jwt})
      : _loginPaths = Map.unmodifiable(loginPaths.map((method, paths) =>
            MapEntry(
                method, paths.map((path) => url.normalize(path)).toList()))),
        _exclude = Map.unmodifiable(exclude.map((method, paths) => MapEntry(
            method,
            paths
                .map((path) => RegExp('^' +
                    (url.normalize(path))
                        .replaceAll(RegExp(r'\{\w+\}'), '[^/]+') +
                    r'$'))
                .toList()))),
        _jwt = jwt;

  shelf.Handler call(shelf.Handler innerHandler) => (shelf.Request request) {
        return Future.sync(() {
          final anonimous = _exclude[request.method] ?? [];
          String jwt = request.headers[HttpHeaders.authorizationHeader];
          if (anonimous.any((regexp) =>
              regexp.hasMatch(url.normalize(request.requestedUri.path)))) {
            if (jwt != null) {
              try {
                request = _addJwtPayloadToRequestContext(request, jwt);
              } on JwtException {}
            }
          } else {
            if (jwt == null) {
              throw (JwtException('Authorization header is not provided'));
            }
            request = _addJwtPayloadToRequestContext(request, jwt);
          }
          return request;
        }).then((request) {
          return Future<shelf.Response>.sync(() => innerHandler(request))
              .then((response) {
            if ((_loginPaths[request.method] ?? [])
                    .contains(url.normalize(request.requestedUri.path)) &&
                response.statusCode == HttpStatus.ok) {
              if (!response.context.containsKey('subject') ||
                  response.context['subject'] == null) {
                return shelf.Response(HttpStatus.unauthorized,
                    body: json.encode({
                      'status': HttpStatus.unauthorized,
                      'message': 'User not found'
                    }),
                    headers: {'content-type': ContentType.json.toString()});
              }
              String token = _jwt.issue(response.context['subject'],
                  payload: response.context['payload']);
              response = _addJwtToResponse(token, response);
            } else if (request.context.containsKey('subject')) {
              String token = _jwt.issue(request.context['subject'],
                  payload: request.context['payload']);
              response = _addJwtToResponse(token, response);
            }
            return response;
          });
        }).catchError((e) {
          if (e is JwtException) {
            return shelf.Response(HttpStatus.unauthorized,
                body: json.encode({
                  'status': HttpStatus.unauthorized,
                  'message': e.toString()
                }),
                headers: {'content-type': ContentType.json.toString()});
          } else {
            throw (e);
          }
        });
      };

  shelf.Request _addJwtPayloadToRequestContext(
      shelf.Request request, String jwt) {
    JwtClaim jwtClaim = _jwt.decode(jwt);
    return request.change(
        context: {'subject': jwtClaim.subject, 'payload': jwtClaim.payload});
  }

  shelf.Response _addJwtToResponse(String jwt, shelf.Response response) =>
      response.change(
          headers: <String, String>{}
            ..addAll(response.headers)
            ..addAll({HttpHeaders.authorizationHeader: jwt}));
}
