import 'dart:io' hide HttpException;
import 'dart:convert';

import 'package:shelf/shelf.dart' as shelf;

import 'http_exception.dart';
import 'middleware.dart';

/// HttpExceptionMiddleware
///
/// Transforms [HttpException] to the corresponding http-responses
class HttpExceptionMiddleware implements Middleware {
  shelf.Handler call(shelf.Handler innerHandler) =>
      (shelf.Request request) async {
        shelf.Response response;
        try {
          response = await innerHandler(request);
        } on shelf.HijackException {
          rethrow;
        } on HttpException catch (e) {
          response = shelf.Response(e.status,
              body: json.encode(e.toMap()),
              headers: {'content-type': ContentType.json.toString()});
        } catch (e, s) {
          response = shelf.Response(HttpStatus.internalServerError,
              body: json.encode({
                'status': HttpStatus.internalServerError,
                'message': '$e',
                'stacktrace': '$s'
              }),
              headers: {'content-type': ContentType.json.toString()});
        }
        return response;
      };
}
