import '../middleware.dart';

/// Abstract annotation Routable
///
/// Base class for [Resource] annotation and methods annotations:
///
/// * [Get],
/// * [Post],
/// * [Put],
/// * [Patch]
/// * [Delete]
abstract class Routable {
  /// Relative path to resource
  final String path;

  /// Middleware for the routable resource
  ///
  /// For a base resource class the middleware is applied to each resource method.
  /// For a method the middleware is applied to the method
  final Middleware middleware;

  const Routable(this.path, this.middleware);
}

/// Resource
///
/// The annotation for resource getters
///
/// Example:
///     class Api {
///       @Resource(path: 'users')
///       Users get users => Users()
///     }
class Resource implements Routable {
  final String path;
  final Middleware middleware;

  /// Creates resource annotation
  const Resource({this.path = '', this.middleware});
}

/// Get
///
/// The annotation of resource method to indicate the method shuold be applied to the resource
/// with `path` while requested with HTTP-method GET
class Get implements Routable {
  final String path;
  final Middleware middleware;

  /// Creates Get annotation
  const Get({this.path = '', this.middleware});
}

/// Post
///
/// The annotation of resource method to indicate the method shuold be applied to the resource
/// with `path` while requested with HTTP-method POST
class Post implements Routable {
  final String path;
  final Middleware middleware;

  /// Create Post annotation
  const Post({this.path = '', this.middleware});
}

/// Put
///
/// The annotation of resource method to indicate the method shuold be applied to the resource
/// with `path` while requested with HTTP-method PUT
class Put implements Routable {
  final String path;
  final Middleware middleware;

  /// Create Put annotation
  const Put({this.path = '', this.middleware});
}

/// Patch
///
/// The annotation of resource method to indicate the method shuold be applied to the resource
/// with `path` while requested with HTTP-method PATCH
class Patch implements Routable {
  final String path;
  final Middleware middleware;

  /// Create Patch annotation
  const Patch({this.path = '', this.middleware});
}

/// Delete
///
/// The annotation of resource method to indicate the method shuold be applied to the resource
/// with `path` while requested with HTTP-method DELETE
class Delete implements Routable {
  final String path;
  final Middleware middleware;

  /// Creates Delete annotation
  const Delete({this.path = '', this.middleware});
}
