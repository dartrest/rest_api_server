import 'dart:io';

import 'package:path/path.dart';
import 'package:shelf/shelf.dart' as shelf;

import 'route.dart';

/// Request router
class Router {
  final List<Route> _routes;
  final String _basePath;

  Router(List<Route> routes, {String basePath = '/'})
      : _routes =
            List.unmodifiable(routes..sort((a, b) => a.path.compareTo(b.path))),
        _basePath = basePath;

  /// Prints routes list
  void printRoutes() {
    _routes.forEach((route) {
      final normalizedPath = url.normalize('$_basePath/${route.path}');
      print('${route.method}\t$normalizedPath');
    });
  }

  /// Router handler
  shelf.Handler get handler {
    return (shelf.Request request) {
      final route = _routes.firstWhere(
          (route) =>
              request.method == route.method &&
              route.pathRegExp.hasMatch(
                  url.relative(request.requestedUri.path, from: _basePath)),
          orElse: () => null);
      if (route == null) {
        return shelf.Response.notFound(
            'Resource ${request.requestedUri} not found',
            headers: {'content-type': ContentType.text.toString()});
      }
      return route.handler(request);
    };
  }
}
