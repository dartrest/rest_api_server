import 'package:get_it/get_it.dart';

final _sl = GetIt.instance;

void register<T>(service, {String instanceName}) {
  if (service is Function) {
    instanceName == null
        ? _sl.registerLazySingleton<T>(service)
        : _sl.registerLazySingleton<T>(service, instanceName: instanceName);
  } else {
    instanceName == null
        ? _sl.registerSingleton<T>(service)
        : _sl.registerSingleton<T>(service, instanceName: instanceName);
  }
}

T locateService<T>({String instanceName}) {
  return instanceName == null ? _sl<T>() : _sl<T>(instanceName: instanceName);
}
