import 'package:build/build.dart';
import 'package:source_gen/source_gen.dart';

import 'src/builder/routes_generator.dart';

Builder resourceInfoBuilder(BuilderOptions options) =>
    LibraryBuilder(ResourceInfoGenerator(),
        formatOutput: (String content) => content,
        generatedExtension: '.resource.info',
        header: '');

Builder routesBuilder(BuilderOptions options) => RoutesBuilder();
