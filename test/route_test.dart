@TestOn('vm')
import 'dart:convert';
import 'package:test/test.dart';
import 'package:rest_api_server/src/route.dart';
import 'package:data_model/data_model.dart';
import 'package:shelf/shelf.dart' as shelf;

void main() {
  group('make Response', () {
    final testEnum = TestEnum('test1');
    test('from DataTime', () async {
      final date = DateTime.parse('2020-03-11 11:23:33');
      shelf.Response resp = await makeResponseFrom(date);
      expect(DateTime.parse(json.decode(await resp.readAsString())).toLocal(),
          date);
    });

    test('from JsonEncodable', () async {
      shelf.Response resp = await makeResponseFrom(testEnum);
      expect(await resp.readAsString(), '"test1"');
    });

    test('from Map', () async {
      Map<TestEnum, dynamic> testMap = {
        TestEnum.test1: 12345,
        TestEnum.test2: 'String',
        TestEnum.test3: null
      };

      shelf.Response resp = await makeResponseFrom(testMap);
      expect(await resp.readAsString(),
          '{"test1":12345,"test2":"String","test3":null}');
    });

    test('Other', () async {
      final testClass = TestClass();

      shelf.Response resp = await makeResponseFrom(testClass);
      expect(await resp.readAsString(), '""');
    });
  });
}

class TestClass {}

class TestEnum implements JsonEncodable {
  static const TestEnum test1 = TestEnum._('test1');

  static const TestEnum test2 = TestEnum._('test2');

  static const TestEnum test3 = TestEnum._('test3');

  final String _string;

  const TestEnum._(String string) : _string = string;

  factory TestEnum(String string) {
    if (string == null) return null;
    if (!values.map((value) => value.json).toList().contains(string))
      return null;
    return TestEnum._(string);
  }

  String get value => _string;

  static List<TestEnum> get values => [test1, test2, test3];
  final Map<String, String> _stingStr = const {
    'test1': 'Test I',
    'test2': 'Test II',
    'test3': 'Test III'
  };

  @override
  bool operator ==(dynamic other) {
    if (other is TestEnum) {
      return other._string == _string;
    }
    return false;
  }

  @override
  int get hashCode => _string.hashCode;
  @override
  String toString() => _stingStr[_string];
  String get json => _string;
}
