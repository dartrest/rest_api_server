@TestOn('vm')

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:shelf/shelf.dart' as shelf;
import 'package:test/test.dart';

import 'package:rest_api_server/auth_middleware.dart';

void main() {
  final jwt = Jwt(
      securityKey: 'qwerty',
      issuer: 'Roga & Kopyta',
      maxAge: Duration(seconds: 1));

  test('anonimous allowed access', () async {
    final authMiddleware = AuthMiddleware(
      exclude: {
        'GET': ['/anonimous']
      },
      jwt: jwt,
    );
    final handler =
        authMiddleware((shelf.Request request) => shelf.Response.ok(''));

    final response = await handler(
        shelf.Request('GET', Uri.parse('http://api.server.com/anonimous')));
    expect(response.statusCode, HttpStatus.ok);
  });

  test('anonimous allowed access to parameterized route', () async {
    final authMiddleware = AuthMiddleware(
      exclude: {
        'POST': ['/bookings/{id}/cancel']
      },
      jwt: jwt,
    );
    final handler =
        authMiddleware((shelf.Request request) => shelf.Response.ok(''));

    final response = await handler(shelf.Request(
        'POST', Uri.parse('http://api.server.com/bookings/123/cancel')));
    expect(response.statusCode, HttpStatus.ok);
  });

  test('anonimous access to restricted resource', () async {
    final authMiddleware = AuthMiddleware(
      jwt: jwt,
    );
    final handler =
        authMiddleware((shelf.Request request) => shelf.Response.ok(''));

    final response = await handler(
        shelf.Request('GET', Uri.parse('http://api.server.com/resticted')));
    expect(response.statusCode, HttpStatus.unauthorized);
    final data = json.decode(await response.readAsString());
    expect(data, {
      'status': HttpStatus.unauthorized,
      'message': 'Authorization header is not provided'
    });
  });

  test('access to anonimous resource with not allowed method', () async {
    final authMiddleware = AuthMiddleware(
      exclude: {
        'GET': ['/anonimous']
      },
      jwt: jwt,
    );
    final handler =
        authMiddleware((shelf.Request request) => shelf.Response.ok(''));

    final response = await handler(
        shelf.Request('POST', Uri.parse('http://api.server.com/anonimous')));
    expect(response.statusCode, HttpStatus.unauthorized);
    final data = json.decode(await response.readAsString());
    expect(data, {
      'status': HttpStatus.unauthorized,
      'message': 'Authorization header is not provided'
    });
  });

  test('login', () async {
    final loginPaths = ['/login'];
    final authMiddleware = AuthMiddleware(
      loginPaths: {
        'POST': loginPaths,
      },
      exclude: {
        'POST': loginPaths,
      },
      jwt: jwt,
    );
    final handler = authMiddleware(
        (shelf.Request request) => shelf.Response.ok('', context: {
              'subject': 'user',
              'payload': {'id': 'user', 'role': 'admin'}
            }));

    final response = await handler(
        shelf.Request('POST', Uri.parse('http://api.server.com/login')));
    expect(response.statusCode, HttpStatus.ok);
    final token = response.headers[HttpHeaders.authorizationHeader];
    expect(token, isNotNull);
    final jwtPayload = json
        .decode(String.fromCharCodes(base64.decode('${token.split('.')[1]}=')));
    expect(jwtPayload['sub'], 'user');
    expect(jwtPayload['pld'], {'id': 'user', 'role': 'admin'});
  });

  test('User not found', () async {
    final loginPaths = ['/login'];
    final authMiddleware = AuthMiddleware(
      loginPaths: {
        'POST': loginPaths,
      },
      exclude: {
        'POST': loginPaths,
      },
      jwt: jwt,
    );
    final handler =
        authMiddleware((shelf.Request request) => shelf.Response.ok(''));

    final response = await handler(
        shelf.Request('POST', Uri.parse('http://api.server.com/login')));
    final body = json.decode(await response.readAsString());
    expect(response.statusCode, HttpStatus.unauthorized);
    expect(
        body, {'status': HttpStatus.unauthorized, 'message': 'User not found'});
  });

  test('authenticated access', () async {
    final authMiddleware = AuthMiddleware(
      jwt: jwt,
    );
    final handler =
        authMiddleware((shelf.Request request) => shelf.Response.ok(''));

    final response = await handler(shelf.Request(
        'GET', Uri.parse('http://api.server.com/resticted'),
        headers: {
          HttpHeaders.authorizationHeader: jwt.issue('subject'),
        }));
    expect(response.statusCode, HttpStatus.ok);
  });

  test('session timeout', () async {
    final authMiddleware = AuthMiddleware(
      jwt: jwt,
    );
    final handler =
        authMiddleware((shelf.Request request) => shelf.Response.ok(''));

    final token = jwt.issue('subject');
    await Future.delayed(Duration(milliseconds: 1000));
    final response = await handler(shelf.Request(
        'GET', Uri.parse('http://api.server.com/resticted'),
        headers: {
          HttpHeaders.authorizationHeader: token,
        }));
    expect(response.statusCode, HttpStatus.unauthorized);
    final data = json.decode(await response.readAsString());
    expect(data,
        {'status': HttpStatus.unauthorized, 'message': 'JWT token expired!'});
  });

  test('trailing slash does not affect exclude paths', () async {
    final authMiddleware = AuthMiddleware(
      exclude: {
        'POST': ['/path'],
      },
      jwt: jwt,
    );
    final request1 =
        shelf.Request('POST', Uri.parse('http://api.server.com/path'));
    final request2 =
        shelf.Request('POST', Uri.parse('http://api.server.com/path/'));
    final shelf.Handler handler =
        authMiddleware((shelf.Request request) => shelf.Response.ok(''));
    final response1 = await handler(request1);
    final response2 = await handler(request2);
    expect(response1.statusCode, equals(response2.statusCode));
  });
}
