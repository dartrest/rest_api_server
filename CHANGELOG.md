## 2.3.2

- List as a parameter of resource method

## 2.3.1

- Function `registerService` get additional parameter `instanceName`
- In resource methods now possible to get request body not only as `Map` but also as `String`

## 2.3.0

- Routes build with namespaces
- Fixed trailing slash bug

## 2.2.3

- Added possibility to exclude parametrized paths from AuthMiddleware check

## 2.2.2

- Fix authmiddleware

## 2.2.1

- Fixed AuthMiddleware

## 2.2.0

- Json-encode maps with non-string keys
- Return all errors in maps

## 2.1.0

- In AuthMiddleware make list of loginPath
- HttpExceptionMiddleware returns additional exception parameters

## 2.0.0

- Removed code for aggregation pipeline builder (code moved to mongo_dart 0.4.0)
- Added routes source code generator

## 1.2.0

- Upgrade to mongo_dart 0.4.0-dev.1

## 1.1.0

- added aggregation pipeline builder

## 1.0.1

- content-type header support
- bugfix: now errors are sent with content-type: application/json

## 1.0.0

- support for boolean query parameters

## 0.1.0

- refactoring

## 0.0.1

- Initial version
